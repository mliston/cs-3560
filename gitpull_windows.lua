-- Slightly modified gitpull for use on windows
-- Tested with Lua for windows (https://code.google.com/p/luaforwindows/) and Windows 7

--#!/usr/local/bin/lua
--print("This is an attempt to use Lua to write a script to pull all of my git repositories") 
--It handles a folder name as an optional command line parameter.

--Features to add:
--1) Check subfolders recursively, with a level restriction (level 1,2,3 etc)

require 'lfs' -- This is a third party rock installed by "luarocks"

--Home isn't an env variable on windows as far as I know
--Set home to the directory your git repos are in
--Note \ needs to be escaped, so your path would look something like:
local home = "C:\\Users\\Matt\\Desktop\\cs3560"

--My system doesn't know where git is, this is the git executable that comes with git bash:
--(Note: extra "" prevents an error with space in the filename)
local gitPath = '"C:\\Program Files (x86)\\Git\\cmd\\git"'

-- You might be able to use the original "git fetch" if the system knows where to find git
local actionCmdFetch = gitPath ..' '..'fetch'
local actionCmdPull = gitPath ..' '..'pull'
local actionCmdStatus = gitPath ..' '..'status'

local pathDefault = home
local statusFileName = "_GitStatusOuput.txt"

-- semicolon (;) is not a command seperator on windows, so changing dir and calling git should be done seperately
-- I think && is the seperator, but cd doens't seem to work from os.execute (but it does using lfs.chdir)
local function changeDirandCallGit(dir, cmd)
	--os.execute("cd")  --print old directory (cd with no parameters is equivalent to pwd)
	lfs.chdir(dir)
	--os.execute("cd")  --print new directory
	os.execute(cmd)
end

local function git_pull(path)
        path = path or pathDefault
        for file in lfs.dir(path) do
                if file ~= "." and file ~= ".." then
                        local f = path..'\\'..file
                        f = string.gsub(f,"\\\\","\\") --Sometimes the command parameter comes in with an ending "\"
                        local attr = lfs.attributes (f)
                        assert (type(attr) == "table")
                        if attr.mode == "directory" then
                                if lfs.attributes (f.."\\.git") then --do this only if it's a git repository
                                        local cmd = actionCmdStatus .. " > "..home.."\\"..statusFileName
                                        --print("==" .. file .. "=======" .. ": " .. cmd)
					
					changeDirandCallGit(f, cmd)
                                        local statusFile = io.open(home.."\\"..statusFileName, "r")
                                        local changes = false;  
                                 for line in statusFile:lines() do
                                                if not(string.find(line,"Changes not staged for commit:") == nil) then                                                          
                                                        changes = true;
                                                        print("There are uncommited changes! The following files have changed:");
                                                elseif not(string.find(line,"modified") == nil) then
                                                        print(line);
                                                elseif not(string.find(line,"deleted") == nil) then
                                                        print(line);
                                                end
                                        end

                                        if changes then
						changeDirandCallGit(f, actionCmdFetch)
                                        else
                                                changeDirandCallGit(f, actionCmdPull)
                                        end
                                else -- No .git subfolder
                                        git_pull(f);
                                end
                        end
                end
        end
end

git_pull(arg[1] or pathDefault); 